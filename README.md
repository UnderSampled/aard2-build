This repository is set up to aid in building the Aard2 dictionary for Android, specifically intended for use by the F-Droid app repository (https://f-droid.org/). The actual Aard2 project can be found at https://github.com/itkach/aard2-android. The source and its dependencies are included here as submodules, so any changes published there will also update here, provided you update said submodule.

After you clone, be sure to run 'git submodule init' and 'git submodule update' to get the sources.

To build, run 'cd aard2-android ; ../gradlew build'
